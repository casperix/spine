1.1.2
- Skeleton render config

1.1.1
- Cache for resource loader

1.1.0
- Fixed: atlas file without spaces
- Spine controller
- Attachment timeline
- Fix: Debug text transform
- Skeleton intersection

1.0.0
- Animation play modes

0.9.0
- back to java 17

0.8.2
- semi-automatic deploy
- sample for test

0.8.0
- kotlin 2.0.0 supported
- java 22 supported
