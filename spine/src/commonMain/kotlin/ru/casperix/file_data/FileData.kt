package ru.casperix.file_data

class FileData {
    val pageList = mutableListOf<PageData>()
}

class PageData {
    val blockList = mutableListOf<BlockData>()
}

class BlockData(val name: String) {
    val values = mutableMapOf<String, String>()
}
