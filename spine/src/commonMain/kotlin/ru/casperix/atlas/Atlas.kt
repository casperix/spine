package ru.casperix.atlas

import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.renderer.pixel_map.PixelMap

class Atlas(
    val pages: List<Page>
)

class Page(
    val pixelMap: PixelMap,
    val info: PageProperties,
    val regions: List<RegionAttributes>,
)

class PageProperties(
    val name: String,
    val size: Dimension2i,
    val format: String,
    val minFilter: String,
    val magFilter: String,
    val repeat: String,
    val pma: Boolean,
)

enum class OrthogonalRotate(val degree: Int) {
    ZERO(0),
    ONE(90),
    TWO(180),
    THREE(270),
}

class RegionAttributes(
    val name: String,
    val index: Int,
    val bounds: Box2i,
    val offsets: List<Int>,
    val rotate: OrthogonalRotate,
    val split: List<Int>,
    val pad: List<Int>,
) {
    val inAtlasBounds
        get() = if (rotate == OrthogonalRotate.ZERO || rotate == OrthogonalRotate.TWO) {
            bounds
        } else {
            Box2i.byDimension(bounds.min, Vector2i(bounds.dimension.y, bounds.dimension.x))
        }
}