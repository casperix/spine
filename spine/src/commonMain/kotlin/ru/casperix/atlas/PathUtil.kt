package ru.casperix.atlas

/**
 * Temp class, before fix "kotlin.io.path.Path" miss-error
 */
object PathUtil {
    val separator = '/'
    fun pathParent(value: String): String? {
        val sep = value.replace('\\', separator)

        val index = sep.lastIndexOf(separator)
        if (index == -1) return null

        return sep.slice(0 until index)
    }
}