package ru.casperix.spine.device

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.atlas.RegionAttributes

class PixelMapRegion(
    val pixels: PixelMap,
    val attributes: RegionAttributes,
) {
    val bounds = attributes.inAtlasBounds
    val rotate = DegreeFloat(attributes.rotate.degree.toFloat())
}