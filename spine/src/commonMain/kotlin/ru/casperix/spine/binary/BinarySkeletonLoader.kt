package ru.casperix.spine.binary

import ru.casperix.atlas.AtlasLoader
import ru.casperix.multiplatform.loader.ResourceLoadError
import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal
import ru.casperix.spine.SkeletonData
import ru.casperix.spine.json.SpineData

object BinarySkeletonLoader {
    fun load(skeletonFile: String, atlasFile:String): EitherFuture<SpineData, ResourceLoadError> {
        val atlasLoader = AtlasLoader.load(atlasFile)
        val spineLoader  = resourceLoader.loadBytes(skeletonFile)

        val signal = EitherSignal<SpineData, ResourceLoadError>()
        spineLoader.thenAccept { bytes->
            atlasLoader.thenAccept { atlas->
                loadBytes(bytes.asUByteArray())
                TODO()
//                val data = SkeletonDecoder(json, skeletonFile, atlas).output
//                signal.accept(Pair(data, atlas))
            }
        }
        spineLoader.thenReject {
            signal.reject(it)
        }
        atlasLoader.thenReject {
            signal.reject(it)
        }

        return signal
    }

    private fun loadBytes(bytes: UByteArray):SkeletonData = BinarySkeletonBuffer(bytes).run {
        //  WHAT ARE SCALE???
        val scale = 1


        val hash = readBytes(8)
        val version = readString()
        val x = readFloat()
        val y = readFloat()
        val width = readFloat()
        val height = readFloat()
        val referenceScale = readFloat() * scale
        val nonessential = readBoolean()
        if (nonessential) {
            val fps = readFloat()
            val imagesPath = readString()
            val audioPath = readString()
            println(audioPath)
        }
        //FAIL!!!
        val stringsAmount = readVarint()
        val stringList = (0 until stringsAmount).map {
            readString()
        }
        val bonesAmount = readVarint().toInt()
        repeat(bonesAmount) {
            val name = readString()
            val parentId = if (it == 0) null else readVarint(true)

            val rotation = readFloat()
            val x = readFloat() * scale
            val y = readFloat() * scale
            val scaleX = readFloat()
            val scaleY = readFloat()
            val shearX = readFloat()
            val shearY = readFloat()
            val length = readFloat() * scale
            val inherit = readVarint(true)
            val skinRequired = readBoolean()
            if (nonessential) {
                val color= readColor()
                val icon = readString()
                val visible = readBoolean()
            }
        }

        TODO()

    }
}
