package ru.casperix.spine.binary

/**
 *  https://ru.esotericsoftware.com/spine-binary-format
 */
object BinarySkeletonConstant {
    const val ATTACHMENT_REGION = 0
    const val ATTACHMENT_BOUNDING_BOX = 1
    const val ATTACHMENT_MESH = 2
    const val ATTACHMENT_LINKED_MESH = 3
    const val ATTACHMENT_PATH = 4
    const val ATTACHMENT_POINT = 5
    const val ATTACHMENT_CLIPPING = 6

    const val BLEND_MODE_NORMAL = 0
    const val BLEND_MODE_ADDITIVE = 1
    const val BLEND_MODE_MULTIPLY = 2
    const val BLEND_MODE_SCREEN = 3

    const val CURVE_LINEAR = 0
    const val CURVE_STEPPED = 1
    const val CURVE_BEZIER = 2

    const val BONE_ROTATE = 0
    const val BONE_TRANSLATE = 1
    const val BONE_SCALE = 2
    const val BONE_SHEAR = 3

    const val TRANSFORM_NORMAL = 0
    const val TRANSFORM_ONLY_TRANSLATION = 1
    const val TRANSFORM_NO_ROTATION_OR_REFLECTION = 2
    const val TRANSFORM_NO_SCALE = 3
    const val TRANSFORM_NO_SCALE_OR_REFLECTION = 4

    const val SLOT_ATTACHMENT = 0
    const val SLOT_COLOR = 1
    const val SLOT_TWO_COLOR = 2

    const val PATH_POSITION = 0
    const val PATH_SPACING = 1
    const val PATH_MIX = 2
    const val PATH_POSITION_FIXED = 0
    const val PATH_POSITION_PERCENT = 1
    const val PATH_SPACING_LENGTH = 0
    const val PATH_SPACING_FIXED = 1
    const val PATH_SPACING_PERCENT = 2
    const val PATH_ROTATE_TANGENT = 0
    const val PATH_ROTATE_CHAIN = 1
    const val PATH_ROTATE_CHAIN_SCALE = 2
}