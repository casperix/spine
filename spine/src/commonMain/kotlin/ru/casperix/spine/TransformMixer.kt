package ru.casperix.spine

import ru.casperix.math.Transform

object TransformMixer {

    fun mix(bone: Transform, target: Transform, offset: Transform, mix: Transform, isLocal: Boolean, isRelative: Boolean): Transform {
        val x = mixValue(mix.x, target.x, offset.x, bone.x, isLocal, isRelative)
        val y = mixValue(mix.y, target.y, offset.y, bone.y, isLocal, isRelative)
        val rotation = mixValue(mix.rotation, target.rotation, offset.rotation, bone.rotation, isLocal, isRelative)
        val shearX = mixValue(mix.shearX, target.shearX, offset.shearX, bone.shearX, isLocal, isRelative)
        val shearY = mixValue(mix.shearY, target.shearY, offset.shearY, bone.shearY, isLocal, isRelative)

        val scaleX = mixScale(mix.scaleX, target.scaleX, offset.scaleX, bone.scaleX, isRelative)
        val scaleY = mixScale(mix.scaleY, target.scaleY, offset.scaleY, bone.scaleY, isRelative)

        return Transform(x, y, rotation, scaleX, scaleY, shearX, shearY)
    }

    private fun mixValue(mix: Float, target: Float, offset: Float, bone: Float, isLocal: Boolean, isRelative: Boolean): Float {
        return bone + (target - bone + offset) * mix
    }

    private fun mixScale(mix: Float, target: Float, offset: Float, bone: Float, relative: Boolean): Float {
        return if (relative) {
            bone * ((target - 1 + offset) * mix + 1)
        } else {
            if (mix != 0f && bone != 0f) (bone + (target - bone + offset) * mix) / bone
            else bone
        }
    }
}