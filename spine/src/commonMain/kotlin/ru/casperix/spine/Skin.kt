package ru.casperix.spine

import ru.casperix.math.color.Color


data class SkinAttachmentKey(val name: String, val slotIndex: Int)

class Skin(
    val name:String,
    val color: Color,
    val attachments: Map<SkinAttachmentKey, Attachment>,
    val bones:List<BoneData>,
    val constraints:List<ConstraintData>
    ) {
    fun getAttachment(slotIndex: Int, attachmentName: String): Attachment? {
        return attachments[SkinAttachmentKey(attachmentName, slotIndex)]
    }
}