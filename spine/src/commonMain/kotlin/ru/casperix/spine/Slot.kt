package ru.casperix.spine

import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4f


class Slot(
    val skeleton: Skeleton,
    val data: SlotData,
    val bone: Bone,
) {
    var attachmentName:String? = null
    var attachment: Attachment? = null
    var color: RgbaColor4f = Color.WHITE.toRGBA()
    var darkColor: RgbaColor4f? = null
    var deform = FloatArray(0)
    var sequenceIndex: Int = -1

    init {
        setToSetupPose()
    }

    fun setToSetupPose() {
        color = data.color
        if (darkColor != null) darkColor = data.darkColor

        setupAttachment(data.attachmentName)
    }

    fun updateAttachment() {
        setupAttachment(attachmentName)
    }

    fun setupAttachment(nextAttachmentName: String?) {
        val nextAttachment = if (nextAttachmentName != null) {
            skeleton.getAttachment( data.index, nextAttachmentName)
        } else null

        if (attachment == null || nextAttachment == null || nextAttachment !is VertexAttachment || attachment !is VertexAttachment || attachment !is TimelineAttachment || nextAttachment !is TimelineAttachment) {
            deform = FloatArray(0)
        }

        attachmentName = nextAttachmentName
        attachment = nextAttachment
        sequenceIndex = -1

    }
}