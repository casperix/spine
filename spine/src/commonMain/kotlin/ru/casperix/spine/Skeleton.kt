package ru.casperix.spine

import ru.casperix.math.Transform
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.spine.animation.Animation

class Skeleton(
    val data: SkeletonData,
) {
    val bones: List<Bone>
    val slots: List<Slot>
    val ikConstraints: List<IkConstraint>
    val pathConstraints: List<PathConstraint>
    val physicsConstraints: List<PhysicsConstraint>
    val transformConstraints: List<TransformConstraint>

    var skins: Set<Skin> = emptySet()
        set(value) {
            field = value
            updateSkins()
        }

    val drawOrder = mutableListOf<Slot>()

    var color: Color = Color.WHITE; private set
    var time: Float = 0f; private set

    var transform: Transform = Transform.IDENTITY

    init {
        val boneCache = mutableListOf<Bone>()
        bones = data.bones.map { boneData ->
            val bone = if (boneData.parent == null) {
                Bone(this, boneData, null)
            } else {
                val parent: Bone = boneCache[boneData.parent.index]
                val bone = Bone(this, boneData, parent)
                parent.children.add(bone)
                bone
            }
            boneCache.add(bone)
            bone
        }

        slots = data.slots.map { slotData ->
            val bone = bones[slotData.boneData.index]
            val slot = Slot(this, slotData, bone)
            drawOrder += slot
            slot
        }

        ikConstraints = data.ikConstraints.map {
            IkConstraint(this, it)
        }

        transformConstraints = data.transformConstraints.map {
            TransformConstraint(this, it)
        }

        pathConstraints = data.pathConstraints.map {
            PathConstraint(this, it)
        }

        physicsConstraints = data.physicConstraints.map {
            PhysicsConstraint(this, it)
        }

        color = RgbaColor4f(1f, 1f, 1f, 1f)

    }
    fun setSkinByNames(names:Collection<String>) {
        skins = names.mapNotNull { name->
            data.skins.firstOrNull { nextSkin -> nextSkin.name == name }
        }.toSet()
    }

    private fun updateSkins() {
        slots.forEach {
            it.updateAttachment()
        }
    }

    fun setToSetupPose() {
        setBonesToSetupPose()
        setSlotsToSetupPose()
    }

    private fun setBonesToSetupPose() {
        bones.forEach (Bone::setToSetupPose)
        transformConstraints.forEach (TransformConstraint::setToSetupPose)
    }

    fun setSlotsToSetupPose() {
        slots.forEach(Slot::setToSetupPose)
    }

    fun setTime(time: Float) {
        this.time = time
    }

    fun updateWorldTransform() {
        bones.forEach {
            it.updateTransform()
        }
        transformConstraints.forEach {
            it.update()
        }
    }

    fun getAttachment(slotIndex: Int, attachmentName: String): Attachment? {
        skins.forEach { skin ->
            skin.getAttachment(slotIndex, attachmentName)?.let { attachment ->
                return attachment
            }
        }
        return null
    }


    fun getAnimation(animationName: String): Animation? {
        return data.getAnimation(animationName)
    }

}