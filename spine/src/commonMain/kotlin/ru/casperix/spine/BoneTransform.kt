package ru.casperix.spine

import kotlinx.serialization.Serializable

@Serializable
enum class BoneTransform {
    normal,
    onlyTranslation,
    noRotationOrReflection,
    noScale,
    noScaleOrReflection
}