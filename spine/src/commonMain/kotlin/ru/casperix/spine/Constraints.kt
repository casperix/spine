package ru.casperix.spine

import ru.casperix.math.Transform

interface ConstraintData {
    val name:String
    var order:Int
    var skinRequired:Boolean
}

class IkConstraintData(
    override val name:String,
    override var order:Int,
    override var skinRequired:Boolean,
) : ConstraintData

class PathConstraintData(
    override val name:String,
    override var order:Int,
    override var skinRequired:Boolean,
) : ConstraintData

class PhysicConstraintData(
    override val name:String,
    override var order:Int,
    override var skinRequired:Boolean,
) : ConstraintData

class TransformConstraintData(
    override val name:String,
    override var order:Int,
    override var skinRequired:Boolean,
    val target:BoneData,
    val bones:List<BoneData>,
    val local:Boolean,
    val relative:Boolean,
    val offset: Transform,
    val mix: Transform,
) : ConstraintData




class IkConstraint(val skeleton: Skeleton, val data:IkConstraintData)

class PathConstraint(val skeleton: Skeleton, val data:PathConstraintData)
class PhysicsConstraint(val skeleton: Skeleton, val data:PhysicConstraintData)
