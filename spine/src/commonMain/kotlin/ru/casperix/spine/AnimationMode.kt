package ru.casperix.spine

enum class AnimationMode {
    PLAY_LOOP,
    PLAY_AND_STOP,
}