package ru.casperix.spine

class Event(
    val data: EventData,
    val time:Float,

    var balance:Float,
    var floatValue: Float,
    var intValue:Int,
    var stringValue:String = "",
    var volume:Float,
)