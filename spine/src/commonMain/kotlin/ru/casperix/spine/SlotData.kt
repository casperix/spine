package ru.casperix.spine

import ru.casperix.math.color.rgba.RgbaColor4f

class SlotData(
    var attachmentName: String? = null,
    var blendMode: BlendMode = BlendMode.normal,
    val boneData: BoneData,
    val color: RgbaColor4f = RgbaColor4f(1f, 1f, 1f, 1f),
    var darkColor: RgbaColor4f? = null,
    val index: Int = 0,
    val name: String = "",
    val path: String? = null,
    var visible: Boolean = true,
)