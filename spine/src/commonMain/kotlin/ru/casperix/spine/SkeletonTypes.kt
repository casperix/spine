package ru.casperix.spine

enum class MixBlend {
    setup,
    first,
    replace,
    add,
}

enum class MixDirection {
    `in`,
    `out`,
}


class EventData


