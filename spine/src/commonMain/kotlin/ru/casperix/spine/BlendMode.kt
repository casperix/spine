package ru.casperix.spine

import kotlinx.serialization.Serializable

@Serializable
enum class BlendMode {
    normal,
    additive,
    multiply,
    screen,
}