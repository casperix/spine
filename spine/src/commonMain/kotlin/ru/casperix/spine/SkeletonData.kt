package ru.casperix.spine

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.spine.animation.Animation

class SkeletonData(
    val animations: List<Animation>,
    val audioPath: String,
    val bones: List<BoneData>,
    val defaultSkin: Skin?,
    val events: List<EventData>,
    val fps: Float,
    val hash: String,
    val ikConstraints: List<IkConstraintData>,
    val imagesPath: String,
    val name: String,
    val pathConstraints: List<PathConstraintData>,
    val physicConstraints: List<PhysicConstraintData>,
    val referenceScale: Float,
    val skins: List<Skin>,
    val slots: List<SlotData>,
    val transformConstraints: List<TransformConstraintData>,
    val version: String,
    val area: Box2f,
) {

    fun getAnimation(animationName: String): Animation? {
        return animations.firstOrNull { it.name == animationName }
    }
}