package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class ValueMode {
    @SerialName("length")
    Length,
    fixed,
    percent
}