package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

/**
 * https://ru.esotericsoftware.com/spine-json-format
 */
@Serializable
class SkeletonJson(
    val skeleton: SkeletonJsonAttributes,
    val animations: Map<String, AnimationJson>,
    val bones: List<BoneJson>,
    val transform: List<TransformConstraintJson>? = null,
    val events: Map<String, EventJson>? = null,
    val skins: List<SkinJson>,
    val slots: List<SlotJson>,
)