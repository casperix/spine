package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("linkedmesh")
@Serializable
class LinkedMeshAttachmentJson(
    override val name:String = "",
    val path:String? = null,
    val skin:String? = null,
    val parent:String,
    val deform:Boolean = true,
    val color: String = "FFFFFFFF",
    val width:Float? = null,
    val height:Float? = null,
) : AttachmentJson