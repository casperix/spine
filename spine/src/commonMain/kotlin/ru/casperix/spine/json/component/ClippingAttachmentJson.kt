package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("clipping")
@Serializable
class ClippingAttachmentJson(
    override val name:String = "",
    val end:String,
    val vertexCount:Int,
    val vertices: List<Float>,
    val color: String = "CE3A3AFF",
) : AttachmentJson