package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable
import ru.casperix.spine.animation.*



@Serializable
class BoneTimelineJson(
    val rotate: List<RotateKeyFrame>? = null,
    val translate: List<TranslateKeyFrame>? = null,
    val translatex: List<TranslateXKeyFrame>? = null,
    val translatey: List<TranslateYKeyFrame>? = null,
    val scale: List<ScaleKeyFrame>? = null,
    val scalex: List<ScaleXKeyFrame>? = null,
    val scaley: List<ScaleYKeyFrame>? = null,
    val shear: List<ShearKeyFrame>? = null,
    val shearx: List<ShearXKeyFrame>? = null,
    val sheary: List<ShearYKeyFrame>? = null,
)

@Serializable
class SlotAttachment(override val time: Float = 0f, val name: String? = null) : KeyFrame

@Serializable
class SlotColorJson(override val time: Float = 0f, val color: String, val curve: Curve = LinearCurve) : KeyFrame

@Serializable
class SlotTwoColorJson(
    override val time: Float = 0f,
    val light: String,
    val dark: String,
    val curve: Curve = LinearCurve,
) : KeyFrame


@Serializable
class SlotTimelineJson(
    val attachment: List<SlotAttachment>? = null,
    val rgba: List<SlotColorJson>? = null,
    val rgb: List<SlotColorJson>? = null,
    val rgba2: List<SlotTwoColorJson>? = null,
    val rgb2: List<SlotTwoColorJson>? = null,
    val alpha: List<ValueKeyFrame>? = null,
)


@Serializable
class IKConstraintKeyFrame(
    override val time: Float = 0f,
    val mix: Float = 1f,
    val softness: Float = 0f,
    val bendPositive: Boolean = false,
    val compress: Boolean = false,
    val stretch: Boolean = false,
) : KeyFrame

typealias IKConstraintTimeline = List<IKConstraintKeyFrame>

@Serializable
class TransformKeyFrame(
    override val time: Float = 0f,
    val rotateMix: Float = 1f,
    val translateMix: Float = 1f,
    val scaleMix: Float = 1f,
    val shearMix: Float = 1f,
) : KeyFrame

typealias TransformConstraintTimeline = List<TransformKeyFrame>

@Serializable
class PathPositionKeyFrame(
    override val time: Float = 0f,
    val position: Float = 1f,
) : KeyFrame

@Serializable
class PathSpacingKeyFrame(
    override val time: Float = 0f,
    val spacing: Float = 1f,
) : KeyFrame

@Serializable
class PathMixKeyFrame(
    override val time: Float = 0f,
    val rotateMix: Float = 1f,
    val translateMix: Float = 1f,
) : KeyFrame

@Serializable
class PathConstraintTimeline(
    val position: List<PathPositionKeyFrame>? = null,
    val spacing: List<PathSpacingKeyFrame>? = null,
    val mix: List<PathMixKeyFrame>? = null,
)

@Serializable
class DeformKeyFrame(
    override val time: Float = 0f,
    val offset: Int = 0,
    val vertices: List<Float>,
    val curve: Curve = LinearCurve,
    val c2: Float = 0f,
    val c3: Float = 1f,
    val c4: Float = 1f,
) : KeyFrame

@Serializable
class EventKeyFrame(
    val name: String,
    /**
     * No default value in documentation.
     * But exist file without field.
     * version: 4.2.22.
     */
    override val time: Float = 0f,
    val int: Int? = null,
    val float: Float? = null,
    val string: String? = null,
    val volume: Float? = null,
    val balance: Float? = null,
) : KeyFrame

typealias EventTimeline = List<EventKeyFrame>

@Serializable
class DrawOrderInfo(
    val slot: String,
    val offset: Int,
)

@Serializable
class DrawOrderKeyFrame(
    override val time: Float = 0f,
    val offsets: List<DrawOrderInfo>,
) : KeyFrame
typealias DrawOrderTimeline = List<DrawOrderKeyFrame>

@Serializable
class AnimationJson(
    val bones: Map<String, BoneTimelineJson>? = null,
    val slots: Map<String, SlotTimelineJson>? = null,
    val ik: Map<String, IKConstraintTimeline>? = null,
    val transform: Map<String, TransformConstraintTimeline>? = null,
    val path: Map<String, PathConstraintTimeline>? = null,
    val deform: Map<String, Map<String, Map<String, DeformKeyFrame>>>? = null,//skin->slot->mesh
    val events: EventTimeline? = null,
    val draworder: Map<String, DrawOrderTimeline>? = null,
)