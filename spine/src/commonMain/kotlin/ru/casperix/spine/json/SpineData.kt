package ru.casperix.spine.json

import ru.casperix.atlas.Atlas
import ru.casperix.spine.SkeletonData

data class SpineData(val skeletonData: SkeletonData, val atlas: Atlas)