package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable
import ru.casperix.spine.BoneTransform
import ru.casperix.spine.Inherit

@Serializable
class BoneJson(
    val name: String,
    val length: Float = 0f,
    val transform: BoneTransform = BoneTransform.normal,
    val inherit: Inherit = Inherit.normal,
    val skin: Boolean = false,
    val x: Float = 0f,
    val y: Float = 0f,
    val rotation: Float = 0f,
    val scaleX: Float = 1f,
    val scaleY: Float = 1f,
    val shearX: Float = 0f,
    val shearY: Float = 0f,
    val color: String = "0x989898FF",
    /**
     * Undocumented section
     */
    val parent:String? = null,
    val visible:Boolean = true,
)