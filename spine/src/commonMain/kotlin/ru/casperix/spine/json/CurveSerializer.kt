package ru.casperix.spine.json

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.*
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.spine.animation.BezierCurve
import ru.casperix.spine.animation.Curve
import ru.casperix.spine.animation.SteppedCurve
import ru.casperix.spine.animation.UnknownCurve
import ru.casperix.spine.json.component.*

class CurveSerializer : KSerializer<Curve> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("Curve")

    override fun deserialize(decoder: Decoder): Curve {
        require(decoder is JsonDecoder)
        when (val element = decoder.decodeJsonElement()) {
            is JsonArray -> {
                val floats = element.jsonArray.map { it.jsonPrimitive.content.toFloat() }
                if (floats.size == 4) {
                     return BezierCurve(Vector2f(floats[0], floats[1]), Vector2f(floats[2], floats[3]))
                } else  {
                    return UnknownCurve
                }
            }

            is JsonPrimitive -> {
                val content = element.content
                if (content == "stepped") {
                    return SteppedCurve
                } else {
                    throw Exception("Unsupported or invalid format")
                }
            }

            else -> {
                throw Exception("Unsupported or invalid format")
            }
        }
    }

    override fun serialize(encoder: Encoder, value: Curve) {
        TODO("Not yet implemented")
    }

}