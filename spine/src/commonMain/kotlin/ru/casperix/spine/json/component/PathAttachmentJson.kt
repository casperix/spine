package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("path")
@Serializable
class PathAttachmentJson(
    override val name:String = "",
    val closed:Boolean = false,
    val constantSpeed:Boolean = true,
    val lengths:List<Float>,
    val vertexCount:Int,
    val vertices: List<Float>,
    val color: String = "FF7F00FF",
) : AttachmentJson