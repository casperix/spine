package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("region")
@Serializable
class RegionAttachmentJson(
    override val name:String = "",
    val path: String? = null,
    val x: Float = 0f,
    val y: Float = 0f,
    val scaleX: Float = 1f,
    val scaleY: Float = 1f,
    val rotation: Float = 0f,
    val width: Float,
    val height: Float,
    val color: String = "FFFFFFFF",
) : AttachmentJson