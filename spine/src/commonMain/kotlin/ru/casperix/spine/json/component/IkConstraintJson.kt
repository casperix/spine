package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

@Serializable
class IkConstraintJson(
    val name: String,
    val order: Int,
    val skin: Boolean = false,
    val bones: List<String>,
    val target: String,
    val mix: Float = 1f,
    val softness: Float = 0f,
    val bendPositive: Boolean = true,
    val compress: Boolean = false,
    val stretch: Boolean = false,
    val uniform: Boolean = false,
)
