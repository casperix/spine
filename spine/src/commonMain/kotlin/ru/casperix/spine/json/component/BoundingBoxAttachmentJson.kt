package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("boundingbox")
@Serializable
class BoundingBoxAttachmentJson(
    override val name:String = "",
    val vertexCount:Int,
    val vertices: List<Float>,
    val color: String = "60F000FF",
) : AttachmentJson