package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable
import ru.casperix.spine.BlendMode

@Serializable
class SlotJson(
    val name: String,
    val bone: String,
    val color: String = "FFFFFFFF",
    val dark: String? = null,
    val attachment: String? = null,
    val blend: BlendMode = BlendMode.normal,
    /**
     * Undocumented section
     */
    val visible:Boolean = true,
)