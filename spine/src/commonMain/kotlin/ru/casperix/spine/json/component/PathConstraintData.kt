package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

@Serializable
class PathConstraintData(
    val name: String,
    val order: Int,
    val skin: Boolean = false,
    val bones: List<String>,
    val target: String,
    val positionMode: ValueMode,
    val spacingMode: ValueMode,
    val rotateMode: ValueMode,

    val rotation: Float = 0f,
    val position: Float = 0f,
    val spacing: Float = 0f,

    val rotateMix: Float = 1f,
    val translateMix: Float = 1f,
)