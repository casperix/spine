package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

@Serializable
class TransformConstraintJson(
    val name: String,
    val order: Int = 0,
    val skin: Boolean = false,
    val bones: List<String>,
    val target: String,

    val local: Boolean = false,
    val relative: Boolean = false,

    val rotation: Float = 0f,
    val x: Float = 0f,
    val y: Float = 0f,
    val scaleX: Float = 0f,
    val scaleY: Float = 0f,
    val shearY: Float = 0f,

    val mixRotate:Float = 1f,
    val mixX:Float = 1f,
    val mixY:Float = 1f,
    val mixScaleX:Float = 1f,
    val mixScaleY:Float = 1f,
    val mixShearY:Float = 1f,

)