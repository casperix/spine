package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

@Serializable
class SkeletonJsonAttributes(
    val hash: String,
    val spine: String,
    val x: Float,
    val y: Float,
    val width: Float,
    val height: Float,
    val fps: Float = 30f,
    val images: String,
    val audio: String,
    /**
     * Undocumented section
     */
    val referenceScale:Float = 100f
)