package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("point")
@Serializable
class PointAttachmentJson(
    override val name:String = "",
    val x:Float = 0f,
    val y:Float = 0f,
    val rotation:Float = 0f,
    val color: String = "F1F100FF",
) : AttachmentJson