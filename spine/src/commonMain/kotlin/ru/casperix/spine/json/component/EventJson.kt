package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

@Serializable
class EventJson(
    val int:Int = 0,
    val float: Float = 0f,
    val string:String? = null,
    val audio:String? = null,
    val volume: Float = 1f,
    val balance: Float = 0f,
)