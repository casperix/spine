package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

@Serializable
class SkinJson(
    val name: String,
    val bones: List<String>? = null,
    val ik: List<String>? = null,
    val transform: List<String>? = null,
    val path: List<String>? = null,
    //  slot-name -> attachment-name
    val attachments: Map<String, Map<String, AttachmentJson>>? = null,
    /**
     * Undocumented section
     */
    val color:String? = null,
)

