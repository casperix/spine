package ru.casperix.spine.json.component

import kotlinx.serialization.Serializable

@Serializable
enum class AttachmentType {
    region,
    mesh,
    linkedmesh,
    boundingbox,
    path,
    point,
    clipping,
}