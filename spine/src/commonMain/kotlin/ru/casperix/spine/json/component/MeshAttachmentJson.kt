package ru.casperix.spine.json.component

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@SerialName("mesh")
@Serializable
class MeshAttachmentJson(
    override val name:String = "",
    val path: String? = null,
    val uvs: List<Float>,
    val triangles: List<Float>,
    val vertices: List<Float>,
    val hull: Int,
    val edges:List<Float>? = null,
    val color: String = "FFFFFFFF",
    val width: Float? = null,
    val height: Float? = null,
) : AttachmentJson