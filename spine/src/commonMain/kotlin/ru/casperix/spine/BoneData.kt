package ru.casperix.spine

import ru.casperix.math.color.Color
import ru.casperix.math.Transform

class BoneData(
    val name: String,
    var local: Transform,
    val parent: BoneData?,
    var color: Color,
    var icon: String,
    val index: Int,
    var inherit: Inherit,
    var length: Float,
    var skinRequired: Boolean,
    var visible: Boolean,
)