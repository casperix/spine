package ru.casperix.spine

import ru.casperix.math.Transform

class TransformConstraint(val skeleton: Skeleton, val data: TransformConstraintData) {
    private val bones = data.bones.map {
        skeleton.bones[it.index]
    }
    private var target = skeleton.bones[data.target.index]
    private var mix = data.mix

    /** Applies the constraint to the constrained bones.  */
    fun update() {
        if (mix == Transform.ZERO) return
        applyBones()
    }

    private fun applyBones() {
        val target = target

        bones.forEach { bone ->
            val transform = TransformMixer.mix(bone.local, target.local, data.offset, data.mix, data.local, data.relative).toABCD()
            bone.updateTransform(transform.toMatrix())
        }
    }

    fun setToSetupPose() {
        target = skeleton.bones[data.target.index]
        mix = data.mix
    }


}

