package ru.casperix.spine

import ru.casperix.math.Transform
import ru.casperix.math.TransformABCD
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f

/**
 *
 *
 * Local transform (Transform)
 * 	float x, y, rotation, scaleX, scaleY, shearX, shearY;
 *
 *  Applied local transform
 * 	float ax, ay, arotation, ascaleX, ascaleY, ashearX, ashearY;
 *
 * 	World transform (TransformABCD)
 * 	float a, b, worldX;
 * 	float c, d, worldY;
 */
class Bone(
    val skeleton: Skeleton,
    val data: BoneData,
    val parent: Bone?,
) {
    var isActive = true

    val children = mutableListOf<Bone>()
    var inherit: Inherit = Inherit.normal

    var local: Transform = data.local
    var world: TransformABCD = TransformABCD.IDENTITY


    fun setToSetupPose() {
        local = data.local
        updateTransform()
    }

    fun updateTransform(custom: Matrix3f = local.toLHSMatrix()) {
        val parent = parent
        if (parent == null) { // Root bone.
            world = TransformABCD.from(custom * skeleton.transform.toLHSMatrix())
            return
        }


        when (inherit) {
            Inherit.normal -> {
                //FOR EVERY
                world = TransformABCD.from(custom * parent.world.toMatrix())
                return
            }

            Inherit.onlyTranslation -> {
                TODO()
//                worldX = pa * x + pb * y + parent.worldX
//                worldY = pc * x + pd * y + parent.worldY
//
//                val rx: Float = (rotation + shearX) * degRad
//                val ry: Float = (rotation + 90 + shearY) * degRad
//                a = cos(rx) * scaleX
//                b = cos(ry) * scaleY
//                c = sin(rx) * scaleX
//                d = sin(ry) * scaleY
            }

            Inherit.noRotationOrReflection -> {
                TODO()
//                worldX = pa * x + pb * y + parent.worldX
//                worldY = pc * x + pd * y + parent.worldY
//
//                var s = pa * pa + pc * pc
//                val prx: Float
//                if (s > 0.0001f) {
//                    s = (abs((pa * pd - pb * pc).toDouble()) / s).toFloat()
//                    pa /= skeleton.scaleX
//                    pc /= skeleton.scaleY
//                    pb = (pc * s).toFloat()
//                    pd = (pa * s).toFloat()
//                    prx = atan2Deg(pc, pa)
//                } else {
//                    pa = 0f
//                    pc = 0f
//                    prx = 90 - atan2Deg(pd, pb)
//                }
//                val rx: Float = (rotation + shearX - prx) * degRad
//                val ry: Float = (rotation + shearY - prx + 90) * degRad
//                val la: Float = cos(rx) * scaleX
//                val lb: Float = cos(ry) * scaleY
//                val lc: Float = sin(rx) * scaleX
//                val ld: Float = sin(ry) * scaleY
//                a = pa * la - pb * lc
//                b = pa * lb - pb * ld
//                c = pc * la + pd * lc
//                d = pc * lb + pd * ld
            }

            Inherit.noScale, Inherit.noScaleOrReflection -> {
                TODO()
//                worldX = pa * x + pb * y + parent.worldX
//                worldY = pc * x + pd * y + parent.worldY
//
//                rotation *= degRad
//                val cos: Float = cos(rotation)
//                val sin: Float = sin(rotation)
//                var za: Float = (pa * cos + pb * sin) / skeleton.scaleX
//                var zc: Float = (pc * cos + pd * sin) / skeleton.scaleY
//                var s = sqrt((za * za + zc * zc).toDouble()).toFloat()
//                if (s > 0.00001f) s = 1 / s
//                za *= s
//                zc *= s
//                s = sqrt((za * za + zc * zc).toDouble()).toFloat()
//                if (inherit === Inherit.noScale && pa * pd - pb * pc < 0 != (skeleton.scaleX < 0 != skeleton.scaleY < 0)) s = -s
//                rotation = PI / 2 + atan2(zc, za)
//                val zb: Float = cos(rotation) * s
//                val zd: Float = sin(rotation) * s
//                shearX *= degRad
//                shearY = (90 + shearY) * degRad
//                val la: Float = cos(shearX) * scaleX
//                val lb: Float = cos(shearY) * scaleY
//                val lc: Float = sin(shearX) * scaleX
//                val ld: Float = sin(shearY) * scaleY
//                a = za * la + zb * lc
//                b = za * lb + zb * ld
//                c = zc * la + zd * lc
//                d = zc * lb + zd * ld
            }
        }
    }
}