package ru.casperix.spine

import ru.casperix.spine.animation.Animation
import ru.casperix.spine.animation.AnimationContext

class AnimationState(var animation: Animation? = null, val mode: AnimationMode = AnimationMode.PLAY_LOOP, val dropAfterStop:Boolean = false) {
    var time: Float = 0f
    var speed:Float = 1f

    fun nextFrame(skeleton: Skeleton, tickInSec: Float):Boolean {
        val lastTime = time
        time += tickInSec * speed

        val animation = animation ?: return true

        if (time > animation.duration) {
            when (mode) {
                AnimationMode.PLAY_LOOP -> time -= animation.duration
                AnimationMode.PLAY_AND_STOP -> {
                    time = animation.duration

                    if (dropAfterStop) {
                        return true
                    }
                }
            }
        }

        applyAnimation(skeleton, animation, lastTime)
        skeleton.updateWorldTransform()
        return false
    }

    fun applyAnimation(skeleton: Skeleton, animation: Animation, lastTime: Float) {
        val events = emptyList<Event>()
        val alpha = 1f // For mixing between the current or setup pose (0) or the animation pose (1).
        val blend = MixBlend.first // How the current or setup pose is mixed with the animation pose.

        val direction = MixDirection.`in` // Whether mixing out to the setup pose or in to the animation pose.

        val context = AnimationContext(skeleton, lastTime, time, events, alpha, blend, direction)
        animation.timelines.forEach {
            it.apply(context)
        }
    }

}