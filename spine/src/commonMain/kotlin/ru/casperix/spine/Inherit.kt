package ru.casperix.spine

enum class Inherit {
    normal,
    onlyTranslation,
    noRotationOrReflection,
    noScale,
    noScaleOrReflection,
}