package ru.casperix.spine

import ru.casperix.math.Transform
import ru.casperix.math.color.Color
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.spine.device.PixelMapRegion

interface Attachment {
    val name: String
}

class UnknownAttachment(override val name: String) : Attachment

class EmptyAttachment(override val name: String) : Attachment

class Sequence

interface HasTextureRegion {
    val color: Color
    val path: String?
    val region: PixelMapRegion
    val sequence: Sequence?
}

class RegionAttachment(
    override val name: String,
    override val color: Color,
    override val path: String?,
    override val sequence: Sequence?,
    override val region: PixelMapRegion,
    val x: Float,
    val y: Float,
    val scaleX: Float,
    val scaleY: Float,
    val rotation: Float,
    val width: Float,
    val height: Float,
    val offset: FloatArray,
    val uvs: FloatArray,
//    val regionHeight:Float,
//    val regionOffsetX:Float,
//    val regionOffsetY:Float,
//    val regionOriginalHeight:Float,
//    val regionOriginalWidth:Float,
//    val regionWidth:Float,
//    val rendererObject:Any,
) : Attachment, HasTextureRegion {
    val regionScaleX = width / region.attributes.bounds.dimension.x
    val regionScaleY = height / region.attributes.bounds.dimension.y

    val transform = lazy {
        Matrix3f.scale(Vector2f(1f, -1f)) *
                Matrix3f.rotate(region.rotate) *
                Transform(x, y, rotation, scaleX * regionScaleX, scaleY * regionScaleY, 0f, 0f).toLHSMatrix()
    }
}

class BoundingBoxAttachment(
    val color: Color,
    name: String,
    bones: IntArray,
    id: Int,
    timelineAttachment: Attachment,
    vertices: FloatArray,
    worldVerticesLength: Int,
) : VertexAttachment(name, bones, id, timelineAttachment, vertices, worldVerticesLength) {

}

open class VertexAttachment(
    override val name: String,
    val bones: IntArray,
    val id: Int,
    val timelineAttachment: Attachment,
    val vertices: FloatArray,
    val worldVerticesLength: Int,
) : Attachment

open class TimelineAttachment(
    override val name: String
) : Attachment