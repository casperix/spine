package ru.casperix.spine.animation

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.color.rgba.RgbaColor4f

interface KeyFrame {
    val time: Float
}

interface CurveKeyFrame : KeyFrame {
    val curve: Curve
}

@Serializable
class ValueKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    val value: Float = 0f,
) : CurveKeyFrame


@Serializable
class RGBKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    val color: RgbColor3f,
) : CurveKeyFrame

@Serializable
class RGBAKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    val color:RgbaColor4f,
) : CurveKeyFrame


@Serializable
class RotateKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    @SerialName("value")
    val angle: Float = 0f,
) : CurveKeyFrame

@Serializable
class TranslateKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    val x: Float = 0f,
    val y: Float = 0f,
) : CurveKeyFrame

@Serializable
class ScaleKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    val x: Float = 1f,
    val y: Float = 1f,
) : CurveKeyFrame

@Serializable
class ShearKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    val x: Float = 0f,
    val y: Float = 0f,
) : CurveKeyFrame

@Serializable
class TranslateXKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    @SerialName("value")
    val x: Float = 0f,
) : CurveKeyFrame

@Serializable
class ScaleXKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    @SerialName("value")
    val x: Float = 1f,
) : CurveKeyFrame

@Serializable
class ShearXKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    @SerialName("value")
    val x: Float = 0f,
) : CurveKeyFrame

@Serializable
class TranslateYKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    @SerialName("value")
    val y: Float = 0f,
) : CurveKeyFrame

@Serializable
class ScaleYKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    @SerialName("value")
    val y: Float = 1f,
) : CurveKeyFrame

@Serializable
class ShearYKeyFrame(
    override val time: Float = 0f,
    override val curve: Curve = LinearCurve,
    @SerialName("value")
    val y: Float = 0f,
) : CurveKeyFrame
