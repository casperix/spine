package ru.casperix.spine.animation

import ru.casperix.spine.MixBlend
import ru.casperix.spine.MixDirection
import ru.casperix.spine.Slot
import ru.casperix.spine.json.component.SlotAttachment

class AttachmentTimeline(
    override val slotIndex: Int,
    override val frames: List<SlotAttachment>,
) : AbstractTimeline<SlotAttachment>(frames), SlotTimeline {
    val attachmentNames: List<String> = frames.map { it.name ?: "" }

    override fun apply(context: AnimationContext) = context.getSlotContext(slotIndex)?.run {
        context.apply {
            if (direction == MixDirection.out) {
                if (blend == MixBlend.setup) setAttachment(slot, slot.data.attachmentName)
                return@run
            }

            if (time < frames[0].time) {
                if (blend == MixBlend.setup || blend == MixBlend.first) {
                    setAttachment(slot, slot.data.attachmentName);
                }
                return@run
            }

            frames.asReversed().forEachIndexed { index, nextFrame ->
                if (time >= nextFrame.time) {
                    val actualIndex = frames.lastIndex - index
                    setAttachment(slot, attachmentNames.getOrNull(actualIndex))
                    return@run
                }
            }
        }
    } ?: Unit

    private fun setAttachment(slot: Slot, attachmentName: String?) {
        slot.setupAttachment(attachmentName)
    }
}