package ru.casperix.spine.animation

class RGBTimeline(
    override val slotIndex: Int,
    override val frames: List<RGBKeyFrame>,
) : AbstractTimeline<RGBKeyFrame>(frames), CurveTimeline1, SlotTimeline {
    private val redChannel = SlotColorChannel(frames, 0, { it.color.red }) { it.red }
    private val greenChannel = SlotColorChannel(frames, 0, { it.color.green }) { it.green }
    private val blueChannel = SlotColorChannel(frames, 0, { it.color.blue }) { it.blue }

    override fun apply(context: AnimationContext) = context.getSlotContext(slotIndex)?.run {
        slot.color = slot.color.copy(
            red = redChannel.getCurrentValue(this),
            green = greenChannel.getCurrentValue(this),
            blue = blueChannel.getCurrentValue(this),
        )
    } ?: Unit

}