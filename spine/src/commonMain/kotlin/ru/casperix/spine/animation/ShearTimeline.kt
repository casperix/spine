package ru.casperix.spine.animation

class ShearTimeline(
    override val boneIndex: Int,
    override val frames: List<ShearKeyFrame>,
) : AbstractTimeline<ShearKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val shearXChannel = BoneTransformChannel(frames, false, 0, { it.x }, { it.shearX })
    private val shearYChannel = BoneTransformChannel(frames, false, 1, { it.y }, { it.shearY })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            shearX = shearXChannel.getCurrentValue(this),
            shearY = shearYChannel.getCurrentValue(this),
        )
    } ?: Unit
}


class ShearXTimeline(
    override val boneIndex: Int,
    override val frames: List<ShearXKeyFrame>,
) : AbstractTimeline<ShearXKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val shearXChannel = BoneTransformChannel(frames, false, 0, { it.x }, { it.shearX })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            shearX = shearXChannel.getCurrentValue(this),
        )
    } ?: Unit
}


class ShearYTimeline(
    override val boneIndex: Int,
    override val frames: List<ShearYKeyFrame>,
) : AbstractTimeline<ShearYKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val shearYChannel = BoneTransformChannel(frames, false, 1, { it.y }, { it.shearY })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            shearY = shearYChannel.getCurrentValue(this),
        )
    } ?: Unit
}

