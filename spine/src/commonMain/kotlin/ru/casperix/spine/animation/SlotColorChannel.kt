package ru.casperix.spine.animation

import ru.casperix.math.color.rgba.RgbaColor4f

class SlotColorChannel<CustomKeyFrame : CurveKeyFrame>(
    frames: List<CustomKeyFrame>,
    val channelIndex: Int,
    val valueByFrame: (CustomKeyFrame) -> Float,
    val valueByColor: (RgbaColor4f) -> Float,
) {
    private val channelFrames = frames.map { AbstractTimeline.createFrame(it.time, valueByFrame(it), it.curve, channelIndex) }

    fun getCurrentValue(slotAnimationContext: SlotAnimationContext): Float = slotAnimationContext.run {
        slotAnimationContext.context.run {
            FrameCalculator.getRelativeValue(
                channelFrames,
                time,
                weight,
                blend,
                valueByColor(slot.color),
                valueByColor(slot.data.color),
                true,
            )
        }
    }
}