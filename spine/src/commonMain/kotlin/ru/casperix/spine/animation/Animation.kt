package ru.casperix.spine.animation

import ru.casperix.spine.Event
import ru.casperix.spine.MixBlend
import ru.casperix.spine.MixDirection
import ru.casperix.spine.Skeleton

class Animation(
    val name: String,
    val timelines: List<Timeline>,
) {
    val duration = timelines.maxOfOrNull { it.duration } ?: 0f

    fun apply(
        skeleton: Skeleton,
        lastTime: Float,
        time: Float,
        loop: Boolean,
        events: List<Event>,
        alpha: Float,
        blend: MixBlend,
        direction: MixDirection
    ) {
        TODO()
    }

    fun hasTimeline(propertyIds: Set<String>): Boolean {
        TODO()
    }


}