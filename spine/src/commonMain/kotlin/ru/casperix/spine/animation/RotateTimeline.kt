package ru.casperix.spine.animation

class RotateTimeline(
    override val boneIndex: Int,
    override val frames: List<RotateKeyFrame>,
) : AbstractTimeline<RotateKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val rotationController = BoneTransformChannel(frames, false, 0, { it.angle }, { it.rotation })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(rotation = rotationController.getCurrentValue(this))
    } ?: Unit
}