package ru.casperix.spine.animation

class ScaleTimeline(
    override val boneIndex: Int,
    override val frames: List<ScaleKeyFrame>,
) : AbstractTimeline<ScaleKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val scaleXChannel = BoneTransformChannel(frames, true, 0, { it.x }, { it.scaleX })
    private val scaleYChannel = BoneTransformChannel(frames, true, 1, { it.y }, { it.scaleY })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            scaleX = scaleXChannel.getCurrentValue(this),
            scaleY = scaleYChannel.getCurrentValue(this),
        )
    } ?: Unit
}


class ScaleXTimeline(
    override val boneIndex: Int,
    override val frames: List<ScaleXKeyFrame>,
) : AbstractTimeline<ScaleXKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val scaleXChannel = BoneTransformChannel(frames, true, 0, { it.x }, { it.scaleX })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            scaleX = scaleXChannel.getCurrentValue(this),
        )
    } ?: Unit
}


class ScaleYTimeline(
    override val boneIndex: Int,
    override val frames: List<ScaleYKeyFrame>,
) : AbstractTimeline<ScaleYKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val scaleYChannel = BoneTransformChannel(frames, true, 1, { it.y }, { it.scaleY })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            scaleY = scaleYChannel.getCurrentValue(this),
        )
    } ?: Unit
}

