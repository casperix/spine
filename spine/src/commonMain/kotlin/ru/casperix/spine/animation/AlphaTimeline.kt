package ru.casperix.spine.animation

class AlphaTimeline(
    override val slotIndex: Int,
    override val frames: List<ValueKeyFrame>,
) : AbstractTimeline<ValueKeyFrame>(frames), CurveTimeline1, SlotTimeline {
    private val alphaChannel = SlotColorChannel(frames, 0, { it.value }) { it.alpha }

    override fun apply(context: AnimationContext) = context.getSlotContext(slotIndex)?.run {
        slot.color = slot.color.copy(alpha = alphaChannel.getCurrentValue(this))
    } ?: Unit

}