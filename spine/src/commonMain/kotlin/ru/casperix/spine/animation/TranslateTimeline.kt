package ru.casperix.spine.animation

class TranslateTimeline(
    override val boneIndex: Int,
    override val frames: List<TranslateKeyFrame>,
) : AbstractTimeline<TranslateKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val xChannel = BoneTransformChannel(frames, false, 0, { it.x }, { it.x })
    private val yChannel = BoneTransformChannel(frames, false, 1, { it.y }, { it.y })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            x = xChannel.getCurrentValue(this),
            y = yChannel.getCurrentValue(this),
        )
    } ?: Unit
}

class TranslateXTimeline(
    override val boneIndex: Int,
    override val frames: List<TranslateXKeyFrame>,
) : AbstractTimeline<TranslateXKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val xChannel = BoneTransformChannel(frames, false, 0, { it.x }, { it.x })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            x = xChannel.getCurrentValue(this),
        )
    } ?: Unit
}

class TranslateYTimeline(
    override val boneIndex: Int,
    override val frames: List<TranslateYKeyFrame>,
) : AbstractTimeline<TranslateYKeyFrame>(frames), CurveTimeline1, BoneTimeline {
    private val yChannel = BoneTransformChannel(frames, false, 1, { it.y }, { it.y })

    override fun apply(context: AnimationContext) = context.getBoneContext(boneIndex)?.run {
        bone.local = bone.local.copy(
            y = yChannel.getCurrentValue(this),
        )
    } ?: Unit
}
