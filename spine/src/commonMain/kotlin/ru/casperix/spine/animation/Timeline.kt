package ru.casperix.spine.animation

import ru.casperix.spine.*

interface Timeline {
    val duration: Float
    val frames: List<KeyFrame>
//    val propertyIds: List<String>

    fun apply(context: AnimationContext)

}

class BoneAnimationContext(
    val bone: Bone,
    val context: AnimationContext,
)

class SlotAnimationContext(
    val slot: Slot,
    val context: AnimationContext,
)

class AnimationContext(
    val skeleton: Skeleton,
    val lastTime: Float,
    val time: Float,
    val events: List<Event>,
    val weight: Float,
    val blend: MixBlend,
    val direction: MixDirection,
) {
    fun getBoneContext(boneIndex: Int): BoneAnimationContext? {
        val bone = skeleton.bones.getOrNull(boneIndex) ?: return null
        if (!bone.isActive) return null
        return BoneAnimationContext(bone, this)
    }

    fun getSlotContext(slotIndex: Int): SlotAnimationContext? {
        val slot = skeleton.slots.getOrNull(slotIndex) ?: return null
        if (!slot.bone.isActive) return null
        return SlotAnimationContext(slot, this)
    }
}

data class ChannelFrame(
    val time: Float,
    val value: Float,
    val curve: Curve,
)

abstract class AbstractTimeline<CustomFrame : KeyFrame>(frames: List<CustomFrame>) : Timeline {
    final override val duration: Float = frames.maxOf { it.time }

    companion object {
        fun createFrame(time: Float, value: Float, curve: Curve, channelIndex: Int = 0): ChannelFrame {
            val actualCurve = curve
//                if (curve is TupleBezierCurve) {
//                when (channelIndex) {
//                    0 -> curve.first
//                    1 -> curve.second
//                    else -> throw Exception("Support only two sub-curve")
//                }
//            } else curve

            return ChannelFrame(time, value, actualCurve)
        }
    }
}

interface BoneTimeline {
    val boneIndex: Int
}

interface CurveTimeline : Timeline {
    override val frames: List<CurveKeyFrame>

    companion object {
        val LINEAR = 0
        val STEPPED = 1
        val BEZIER = 2
        val BEZIER_SIZE = 18
    }
}

interface CurveTimeline1 : CurveTimeline {
    companion object {
        const val VALUE = 1
        const val ENTRIES = 2
    }
}

