package ru.casperix.spine.animation

import ru.casperix.math.Transform

class BoneTransformChannel<CustomKeyFrame : CurveKeyFrame>(
    frames: List<CustomKeyFrame>,
    val isScaleMode:Boolean,
    val channelIndex:Int,
    val valueByFrame: (CustomKeyFrame) -> Float,
    val valueByTransform: (Transform) -> Float,
) {
    private val channelFrames = frames.map { AbstractTimeline.createFrame(it.time, valueByFrame(it), it.curve, channelIndex) }

    fun getCurrentValue(boneAnimationContext: BoneAnimationContext): Float = boneAnimationContext.run {
        boneAnimationContext.context.run {
            FrameCalculator.getRelativeValue(
                channelFrames,
                time,
                weight,
                blend,
                valueByTransform(bone.local),
                valueByTransform(bone.data.local),
                isScaleMode
            )
        }
    }
}