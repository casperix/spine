package ru.casperix.math

import ru.casperix.math.geometry.fDEGREE_TO_RADIAN
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.quad_matrix.float32.Matrix3f
import kotlin.math.cos
import kotlin.math.sin

data class Transform(
    val x: Float = 0f,
    val y: Float = 0f,
    val rotation: Float = 0f,
    val scaleX: Float = 1f,
    val scaleY: Float = 1f,
    val shearX: Float = 0f,
    val shearY: Float = 0f,
) {
    companion object {
        val ZERO = Transform(0f, 0f, 0f, 0f, 0f, 0f, 0f)
        val IDENTITY = Transform(0f, 0f, 0f, 1f, 1f, 0f, 0f)
    }

    fun toLHSMatrix(): Matrix3f {
        return toABCD().toMatrix()
    }

    fun toABCD(): TransformABCD {
        val rx: Float = (rotation + shearX) * fDEGREE_TO_RADIAN
        val ry: Float = (rotation + 90f + shearY) * fDEGREE_TO_RADIAN
        return TransformABCD(
            x, y,
            cos(rx) * scaleX,
            cos(ry) * scaleY,
            sin(rx) * scaleX,
            sin(ry) * scaleY,
        )
    }
}