package ru.casperix.math

import ru.casperix.math.quad_matrix.float32.Matrix3f

data class TransformABCD(
    val x: Float,
    val y: Float,
    val a: Float,
    val b: Float,
    val c: Float,
    val d: Float,
) {
    fun toMatrix(): Matrix3f {
        return Matrix3f(
            floatArrayOf(
                a, c, 0f,
                b, d, 0f,
                x, y, 1f,
            )
        )
    }

    companion object {
        val IDENTITY = TransformABCD(0f, 0f, 1f, 0f, 0f, 1f)
        fun from(matrix: Matrix3f) = matrix.run {
            TransformABCD(
                data[6], data[7],
                data[0], data[3],
                data[1], data[4],
            )
        }
    }
}
