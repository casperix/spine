package ru.casperix

import ru.casperix.spine.Skeleton
import ru.casperix.spine.binary.BinarySkeletonLoader
import ru.casperix.spine.json.JsonSkeletonLoader
import kotlin.test.Test
import kotlin.test.assertTrue

class SkeletonLoaderTest {
    @Test
    fun spineLoad() {
        val basePathList = listOf(
//            Pair("spine/hotrod/gear", "spine/hotrod/gear"),
            Pair("spine/spineboy-ess.json", "spine/spineboy.atlas"),
            Pair("spine/spineboy-ess.skel", "spine/spineboy.atlas"),
            Pair("spine/spineboy-pro.json", "spine/spineboy.atlas"),
            Pair("spine/hero-ess.json", "spine/hero.atlas"),
            Pair("spine/hero-pro.json", "spine/hero.atlas"),
            Pair("spine/goblins-ess.json", "spine/goblins.atlas"),
            Pair("spine/goblins-pro.json", "spine/goblins.atlas"),
            Pair("spine/tank-pro.json", "spine/tank.atlas"),
            Pair("spine/mix-and-match-pro.json", "spine/mix-and-match.atlas"),
        )
        basePathList.forEach { (skeletonFile, atlasFile) ->
            if (skeletonFile.endsWith(".json")) {
                JsonSkeletonLoader.load(skeletonFile, atlasFile).then({ (data, atlas) ->
                    val skeleton = Skeleton(data)
                    println("Completed ($skeleton)")
                }, {
                    assertTrue(false, "Error for asset: $skeletonFile\n$it")
                })
            } else {
                BinarySkeletonLoader.load(skeletonFile, atlasFile).then({ (data, atlas) ->
                    val skeleton = Skeleton(data)
                    println("Completed ($skeleton)")
                }, {
                    assertTrue(false, "Error for asset: $skeletonFile\n$it")
                })
            }


        }
    }

}