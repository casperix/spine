package ru.casperix

import ru.casperix.atlas.AtlasLoader
import ru.casperix.file_data.FileDataLoader
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.renderer.pixel_map.PixelMap
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class AtlasLoaderTest {

    @Test
    fun dataParse() {
        val assets = listOf(
            "spine/hero.atlas",
            "spine/chibi-stickers.atlas",
        )

        assets.forEach {
            resourceLoader.loadText(it).thenAccept { raw ->
                val data = FileDataLoader.parse(raw)

                val rawAgain = data.pageList.map { page ->
                    page.blockList.flatMap { block ->
                        listOf(block.name) + block.values.map { item ->
                            item.key + ":" + item.value
                        }
                    }.joinToString("\n")
                }.joinToString("\n\n")

                val rawLines = raw.split(Regex("(\\r\\n|\\r|\\n)")).filter { it.isNotEmpty() }
                val rawAgainLines = rawAgain.split(Regex("(\\r\\n|\\r|\\n)")).filter { it.isNotEmpty() }

                //  Field order unknown.
                //  So we can't direct check
                assertEquals(rawLines.size, rawAgainLines.size)
            }
        }
    }

    @Test
    fun atlasLoad() {
        val assets = listOf(
            "spine/chibi-stickers.atlas",
            "spine/hero.atlas",
        )
        assets.forEach { asset ->
            AtlasLoader.load(asset).then({ atlas ->
                println("Completed ($asset)")
                atlas.pages.forEach { page ->
                    page.regions.forEach { region ->
                        pixelMapRegion(page.pixelMap, region.inAtlasBounds)?.let { regionMap ->
                            resourceLoader.saveImage("temp/" + region.name + ".png", regionMap)
                        }
                    }
                }
            }, {
                assertTrue(false, "Error for asset: $asset\n$it")
            })
        }
    }


    private fun pixelMapRegion(map: PixelMap, region: Box2i): PixelMap? {
        val mapArea = Box2i.byDimension(Vector2i.ZERO, map.dimension.toVector2i())
        if (mapArea.isOutside(region.min) || mapArea.isOutside(region.max)) {
            return null
        }

        val sourceData = map.RGBA() ?: return null
        val target = PixelMap.RGBA(region.dimension.toDimension2i())
        val targetData = target.RGBA()!!

        //extremely slow, but work
        region.iterator().forEach { pixelPos ->
            targetData.set(pixelPos - region.min, sourceData.get(pixelPos))
        }

        return target
    }
}