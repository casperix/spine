package ru.casperix

import ru.casperix.math.BezierCalculation
import ru.casperix.math.curve.float32.BezierCubic2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.toPrecision
import ru.casperix.spine.animation.BezierCurve
import ru.casperix.spine.animation.ChannelFrame
import kotlin.math.absoluteValue
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.time.measureTime

class BezierCalculationTest {
    val curve = BezierCubic2f(Vector2f(0f, 0f), Vector2f(0f, 1f), Vector2f(1f, 1f), Vector2f(1f, 0f))
    val REPEATS = 20_000

    @Test
    fun bezierCacheError() {
        val A = Vector2f(0f, 0f)
        val B = Vector2f(5f, 5f)
        val C = Vector2f(15f, 5f)
        val D = Vector2f(20f, 0f)

        val original = BezierCubic2f(A, B, C, D)
        val curve = BezierCurve(B, C)


        val STEPS = 1000
        val LENGTH = 20f
        val maxError = (0..STEPS).maxOf { step ->
            val x = LENGTH * step / STEPS.toFloat()
            val answerA = BezierCalculation.getApproximateErrorHeight(original, x, 0.0001f)
            val answerB = curve.getValue(ChannelFrame(A.x, A.y, curve), ChannelFrame(D.x, D.y, curve), x)

            (answerA - answerB).absoluteValue
        }
        println("Max error: $maxError")
        assertTrue(maxError < 0.01f)
    }

    @Test
    fun qualityTest() {

        listOf(0f, 0.01f, 0.1f, 0.5f, 1f).forEach { testX ->
            println("value: ${testX.toPrecision(2)}")
            listOf(1, 2, 4, 8, 16, 32, 64, 128, 256, 512).forEach { steps ->
                val dh = BezierCalculation.getDirectHeight(curve, testX, steps)
                val ah = BezierCalculation.getApproximateIterationHeight(curve, testX, steps)
                println("steps: $steps => direct / approx-iter => ${dh.toPrecision(6)} / ${ah.toPrecision(6)} ")
            }

            listOf(0.1f, 0.01f, 0.001f).forEach { error ->
                val result = BezierCalculation.getApproximateErrorHeightPrecision(curve, testX, error)
                println("steps: ?? => error: $error => approx-error-precision => ${result.toPrecision(6)}")
            }

            listOf(0.01f, 0.001f, 0.0001f).forEach { error ->
                val result2 = BezierCalculation.getApproximateErrorHeight(curve, testX, error)
                println("steps: ?? => error: $error => approx-error => ${result2.toPrecision(6)}")
            }
        }

    }

    @Test
    fun performanceDirectHeight() {
        repeat(10) {
            val time1 = measureTime {
                repeat(REPEATS) {
                    val factor = it / REPEATS.toFloat()
                    BezierCalculation.getDirectHeight(curve, factor, 16)
                }
            }

            val time2 = measureTime {
                repeat(REPEATS) {
                    val factor = it / REPEATS.toFloat()
                    BezierCalculation.getApproximateIterationHeight(curve, factor, 16)
                }
            }

            val time3 = measureTime {
                repeat(REPEATS) {
                    val factor = it / REPEATS.toFloat()
                    BezierCalculation.getApproximateErrorHeightPrecision(curve, factor, 0.001f)
                }
            }

            val time4 = measureTime {
                repeat(REPEATS) {
                    val factor = it / REPEATS.toFloat()
                    BezierCalculation.getApproximateErrorHeight(curve, factor, 0.0001f)
                }
            }

            val time5 = measureTime {
                val custom = BezierCurve(curve.p1, curve.p2)

                val f1 = ChannelFrame(curve.p0.x, curve.p0.y, custom)
                val f2 = ChannelFrame(curve.p3.x, curve.p3.y, custom)

                repeat(REPEATS) {
                    val factor = it / REPEATS.toFloat()
                    custom.getValue(f1, f2, factor)
                }
            }

            if (it == 1) {
                println("getDirectHeight: ${time1.inWholeMicroseconds}mks.")
                println("getApproximateIterationHeight: ${time2.inWholeMicroseconds}mks.")
                println("getApproximateErrorHeightPrecision: ${time3.inWholeMicroseconds}mks.")
                println("getApproximateErrorHeight: ${time4.inWholeMicroseconds}mks.")
                println("BezierCurve::getValue: ${time5.inWholeMicroseconds}mks.")
            }
        }
    }
}