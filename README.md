# spine

Support for Spine ( https://ru.esotericsoftware.com/ ) on Kotlin.

Use [JsonSkeletonLoader.kt](spine%2Fsrc%2FcommonMain%2Fkotlin%2Fru%2Fcasperix%2Fspine%2Fjson%2FJsonSkeletonLoader.kt) for asset loading.

See `[sample](sample) project as an example of use.

Note that the library presents a minimal set of features. You can use your own loader or your renderer if you want.

## Deploy
### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator):
`sonatypeStagingProfileId`,
`sonatypeUsername`,
`sonatypePassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.

Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin