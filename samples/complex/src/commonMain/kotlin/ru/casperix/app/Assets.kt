package ru.casperix.app

object Assets {
    val all = listOf(
        SpineInfo("particles/DustParticle.spine-json", "particles/all_hotrod_cars_v2.atlas.txt", "sps"),
        SpineInfo("particles/newParticle1.spine-json", "particles/all_hotrod_cars_v2.atlas.txt", "sps"),
        SpineInfo("particles/newParticle2.spine-json", "particles/all_hotrod_cars_v2.atlas.txt", "sps"),
        SpineInfo("simple/skeleton.spine-json", "simple/simple.atlas.txt", "walk"),
        SpineInfo("gear/gear.spine-json", "gear/all_hotrod_cars_v1.atlas.txt", "walk"),
        SpineInfo("alien/alien-ess.json", "alien/alien.atlas", "walk"),
        SpineInfo("alien/alien-pro.json", "alien/alien.atlas", "walk"),
        SpineInfo("spineboy/spineboy-ess.json", "spineboy/spineboy.atlas", "walk"),
        SpineInfo("spineboy/spineboy-pro.json", "spineboy/spineboy.atlas", "walk"),
        SpineInfo("goblins/goblins-ess.json", "goblins/goblins.atlas", "walk"),
    )
}