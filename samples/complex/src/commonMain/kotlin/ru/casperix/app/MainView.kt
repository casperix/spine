package ru.casperix.app

import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.slider.Slider
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.math.vector.float32.Vector2f

class MainView {
    val largeButtonSkin = SkinProvider.skin.toggleButton.copy(defaultSize = Vector2f(300f, 40f))
    val smallButtonSkin = SkinProvider.skin.toggleButton//.copy(defaultSize = Vector2f(100f, 40f))

    val assetContainer = Container(Layout.VERTICAL)
    val skinContainer = Container(Layout.VERTICAL)
    val animationContainer = Container(Layout.VERTICAL)
    val infoText = Label()

    val pauseButton = ToggleButton("||", skin = smallButtonSkin)
    val playSlowButton = ToggleButton("|>", skin = smallButtonSkin)
    val playNormalButton = ToggleButton(">", skin = smallButtonSkin)
    val playFastButton = ToggleButton(">>", skin = smallButtonSkin)

    val setupButton = PushButton("setup pose")

    val x1Button = ToggleButton("1x1", skin = smallButtonSkin)
    val x100Button = ToggleButton("10x10", skin = smallButtonSkin)
    val x10000Button = ToggleButton("100x100", skin = smallButtonSkin)

    val debugButton = ToggleButton("debug")
    val touchArea = Slider()

    val root = Container(
        Layout.HORIZONTAL,
        assetContainer.setSizeMode(SizeMode.column),
        Container(
            Layout.VERTICAL,
            Empty().setSizeMode(SizeMode.max),
            touchArea.setSizeMode(MaxContentOrViewDimension, FixedDimension(32f)),
        ).setSizeMode(SizeMode.max),
        Panel(
            Layout.HORIZONTAL,
            Container(
                Layout.VERTICAL,
                Label("Skins:"),
                skinContainer.setSizeMode(MaxContentOrViewDimension, MinContentOrViewDimension),
                Label("Animations:"),
                animationContainer.setSizeMode(SizeMode.column),
            ).setSizeMode(SizeMode.column),
            Container(
                Layout.VERTICAL,
                infoText,
                ToggleGroup(
                    Layout.VERTICAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    pauseButton,
                    playSlowButton,
                    playNormalButton,
                    playFastButton
                ),
                setupButton,
                debugButton,
                ToggleGroup(
                    Layout.VERTICAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    x1Button,
                    x100Button,
                    x10000Button,
                )
            ).setSizeMode(SizeMode.column),
        ).setSizeMode(SizeMode.column),
    ).setSizeMode(SizeMode.max)
}