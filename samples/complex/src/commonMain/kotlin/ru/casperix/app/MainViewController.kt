package ru.casperix.app

import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.element.ElementUpdate
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.misc.toPrecision
import ru.casperix.spine.json.JsonSkeletonLoader
import ru.casperix.spine.renderer.SpineController
import kotlin.math.roundToInt
import kotlin.math.roundToLong
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class MainViewController(val view: MainView) {
    var controller: SpineController? = null
    var spineDebug = false
    var sceneWidth = 1

    init {
        view.apply {
            x1Button.isChecked = true

            x1Button.setListenerForChecked { sceneWidth = 1 }
            x100Button.setListenerForChecked { sceneWidth = 10 }
            x10000Button.setListenerForChecked { sceneWidth = 100 }

            pauseButton.setListenerForChecked { controller?.speed = 0f }
            playSlowButton.setListenerForChecked { controller?.speed = 0.2f }
            playNormalButton.setListenerForChecked { controller?.speed = 1f }
            playFastButton.setListenerForChecked { controller?.speed = 3f }

            setupButton.setListener { controller?.skeleton?.setToSetupPose() }
            debugButton.setProperty(spineDebug) { spineDebug = it }

            assetContainer.add(ToggleGroup(Layout.VERTICAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                Assets.all.map {
                    ToggleButton(it.skeletonPath, false, largeButtonSkin) { if (isChecked) setSpine(it) }
                }
            ).setSizeMode(SizeMode.column))

            touchArea.touchSignal.then {
                val controller = controller ?: return@then
//            if (!it) {
//                controller.speed = 1f
//            }
            }

            touchArea.selectSignal.then {
                if (touchArea.touchSignal.value) {
                    controller?.apply {
                        setFixedFrame(it)
                    }
                }
            }
        }
        view.root.update = ElementUpdate {
            nextFrame(it)
        }
    }

    private fun SpineController.setFixedFrame(frameFactor: Float) {
        if (animationList.isEmpty()) return
        val duration = animationList.maxOf { it.animation?.duration ?: 0f }

        val time = frameFactor * duration

        skeleton.setToSetupPose()
        skeleton.setTime(0f)

        speed = 1f

        animationList.forEach {
            it.time = 0f
            it.speed = 1f
        }

        val tick = ((1000f * time).roundToLong()).milliseconds
        nextFrame(tick)

        speed = 0f

    }

    private fun setSpine(info: SpineInfo) {
        JsonSkeletonLoader.load(info.skeletonPath, info.atlasPath).then({
            SpineController(it).apply {
                controller = this
                speed = 0.5f
            }
            rebuildSpineControl()
        }, {
            throw Exception(it.toString())
        })
    }

    private fun rebuildSpineControl() {
        rebuildAnimationButtons()
        rebuildSkinButtons()
    }


    private fun rebuildAnimationButtons() {
        val controller = controller ?: return

        val animations = controller.skeleton.data.animations
        view.animationContainer.children.clear()
        view.animationContainer.children += animations.map {
            ToggleButton(it.name, false) {
                if (isChecked) {
                    controller.addAnimation(it)
                } else {
                    controller.removeAnimation(it)
                }
            }
        }.wrapNodes()
    }

    private fun rebuildSkinButtons() {
        val controller = controller ?: return

        val skins = controller.skeleton.data.skins
        view.skinContainer.children.clear()
        view.skinContainer.children += skins.map {
            ToggleButton(it.name, false) {
                if (isChecked) {
                    controller.skeleton.skins += it
                } else {
                    controller.skeleton.skins -= it
                }
            }
        }.wrapNodes()
    }

    private var accumulated = Duration.ZERO

    fun nextFrame(tick: Duration) {
        val info = listOfNotNull(
            "" + (1000f / tick.inWholeMilliseconds).toPrecision(1) + " FPS",
            controller?.run {
                "" + (skeleton.time * skeleton.data.fps).toPrecision(2) + " frame"
            }
        )

        accumulated += tick
        if (accumulated > 1.seconds) {
            accumulated -= 1.seconds
            view.infoText.text = info.joinToString("\n")
        }

        controller?.apply {
            getAnimationTime(this)?.let {
                view.touchArea.selectSignal.value = it
            }
        }

        if (controller == null || controller?.animationList?.isEmpty() == true) {
            view.touchArea.selectSignal.value = 0f
        }
    }


    private fun getAnimationTime(controller: SpineController): Float? {
        if (view.touchArea.touchSignal.value) return null
        if (controller.animationList.isEmpty()) return null

        val animationState = controller.animationList.firstOrNull { it.animation != null } ?: return null
        val animation = animationState.animation!!

        val duration = animation.duration

        return animationState.time / duration
    }


}