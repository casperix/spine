package ru.casperix.app

class SpineInfo(val skeletonPath: String, val atlasPath: String, val animationName: String)