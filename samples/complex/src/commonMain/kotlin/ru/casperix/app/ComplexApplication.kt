package ru.casperix.app

import ru.casperix.app.surface.DesktopWindow
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.demo_platform.camera.CameraConfig
import ru.casperix.demo_platform.camera.OrthographicCameraController
import ru.casperix.input.InputEvent
import ru.casperix.light_ui.Stage
import ru.casperix.light_ui.node.Node
import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.camera.OrthographicCamera2f
import ru.casperix.math.color.Colors
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Environment
import kotlin.time.Duration


class ComplexApplication(val surface: Surface) : SurfaceListener {
    val renderer = OpenGlRenderer2d()
    val rootNode = Node()
    val stage = Stage(rootNode)
    val view = MainView()

    private val cameraController =
        OrthographicCameraController(OrthographicCamera2f(0.5f, Vector2f.ZERO, Dimension2f.ZERO), CameraConfig(zoomRanges = 0.1f..10f))

    val viewController = MainViewController(view)

    init {
        if (surface is DesktopWindow) {
            surface.maximize()
        }
        surface.setVerticalSync(true)

        rootNode.element = view.root
    }

    override fun dispose() {

    }

    override fun input(event: InputEvent) {
        stage.input(event)

        cameraController.input(event)
    }

    override fun nextFrame(tick: Duration) {

        renderer.viewPort = surface.getSize()
        cameraController.camera.viewport = surface.getSize().toDimension2f()

        renderer.environment = Environment(
            backgroundColor = Colors.NAVY,
            projectionMatrix = cameraController.camera.transform.projection,
            viewMatrix = cameraController.camera.transform.view,
            ambientColor = Colors.BLACK,
            lightPosition = Vector3f(1000f),
        )

        renderer.clear()

        val maxSize = viewController.sceneWidth
        val timeFactor = 1.0 / (maxSize * maxSize).toDouble()

        repeat(maxSize) { x ->
            repeat(maxSize) { y ->
                viewController.controller?.nextFrame(tick * timeFactor)
                viewController.controller?.worldMatrix = Matrix3f.translate(Vector2f(x * 200f, y * 200f))
                viewController.controller?.render(renderer, viewController.spineDebug)
            }
        }

        renderer.flush()

        renderer.environment = Environment(
            backgroundColor = Colors.NAVY,
            projectionMatrix = Matrix3f.orthographic(surface.getSize(), true),
            viewMatrix = Matrix3f.IDENTITY,
            ambientColor = Colors.BLACK,
            lightPosition = Vector3f(1000f),
        )

        stage.render(renderer, tick)
        renderer.flush()
    }


}