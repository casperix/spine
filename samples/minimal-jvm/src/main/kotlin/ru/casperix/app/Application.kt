package ru.casperix.app

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.input.PointerMove
import ru.casperix.math.color.Color
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.text.drawText
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.spine.intersection.SkeletonIntersection
import ru.casperix.spine.json.JsonSkeletonLoader
import ru.casperix.spine.renderer.SpineController
import kotlin.time.Duration

class Application(val surface: Surface) : SurfaceListener {
    val renderer = OpenGlRenderer2d()

    private var controller: SpineController? = null
    private var touchPosition:Vector2f? = null

    class SpineInfo(val skeletonPath: String, val atlasPath: String, val animationName: String)

    init {
        val info = SpineInfo("spineboy/spineboy-ess.json", "spineboy/spineboy.atlas", "walk")


        JsonSkeletonLoader.load(info.skeletonPath, info.atlasPath).then({
            SpineController(it).apply {
                controller = this
                worldMatrix =  Matrix3f.translate(Vector2f(300f, -600f))*Matrix3f.scale(Vector2f(1f, -1f))
                addAnimationIfExist(info.animationName)
            }
        }, {
            throw Exception(it.toString())
        })
    }

    override fun dispose() {

    }

    override fun input(event: InputEvent) {
        if (event is PointerMove) {
            touchPosition = event.position
        }
    }

    override fun nextFrame(tick: Duration) {
        val controller = controller
        val touchPosition = touchPosition

        controller?.nextFrame(tick)

        renderer.environment.apply {
            viewPort = surface.getSize()
            clearColor = Color.NAVY.toRGBA()
            projectionMatrix = Matrix3f.orthographic(surface.getSize(), true)
//            ambientColor = Colors.BLACK
//            lightPosition = Vector3f(1000f)
        }

        val stateInfo = if (controller == null) {
            "No controller"
        } else if (touchPosition == null) {
            "No touch"
        } else {
            val intersection = SkeletonIntersection.getAll(controller, touchPosition).toList()
            if (intersection.isEmpty()) {
                "No intersection"
            } else {
                intersection.map {
                    "Bone: " + it.slot.bone.data.name + "; " + "Slot: " + it.slot.data.name
                }.joinToString("\n")
            }
        }

        renderer.clear()
        controller?.render(renderer, false)
        renderer.drawText(stateInfo, Matrix3f.IDENTITY)
        renderer.flush()
    }

}